var net = require("net");
var JSONStream = require('JSONStream');

var serverHost = process.argv[2];
var serverPort = process.argv[3];
var botName = process.argv[4];
var botKey = process.argv[5];

var throttle = 0.5;
var throttleAcc = 0.001;
var counter = 100;
var skidAngle = 0.0;

console.log("I'm", botName, "v.01", "and connect to", serverHost + ":" + serverPort);

client = net.connect(serverPort, serverHost, function() {
  return send({
    msgType: "join",
    data: {
      name: botName,
      key: botKey
    }
  });
});

function send(json) {
  client.write(JSON.stringify(json));
  return client.write('\n');
};

jsonStream = client.pipe(JSONStream.parse());

jsonStream.on('data', function(data) {
  if (data.msgType === 'carPositions') {
    if (counter == 100) {
      console.log("throttle: ", throttle);
      console.log("skidAngle: ", skidAngle);
      counter = 0;
    }
    counter++;
    throttle += throttleAcc;
    skidAngle = Math.abs(data.data[0].angle);    
    throttle -= skidAngle / 360.0 * 0.8;
    if (throttle > 1.0) {
      throttle = 1.0;
    }
    if (throttle < 0.5) {
      throttle = 0.5;
    }
    send({
      msgType: "throttle",
      data: throttle
    });
  } else {
    if (data.msgType === 'join') {
      console.log('Joined')
    } else if (data.msgType === 'gameStart') {
      console.log('Race started');
    } else if (data.msgType === 'gameEnd') {
      console.log('Race ended');
    } else if (data.msgType === 'crash') {
      console.log('Crashed');
      throttle = 0.0;
    } else if (data.msgType === 'spawn') {
      console.log('Spawned');
      throttle = 0.5;
    } else if (data.msgType === 'finish') {
      console.log('Finished')
      throttle = 0.0;
    } else if (data.msgType === 'dnf') {
      console.log('dnf');
    }
    send({
      msgType: "ping",
      data: {}
    });
  }
});

jsonStream.on('error', function() {
  return console.log("disconnected");
});
